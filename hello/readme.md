

## hello-world 실행
```
docker run hello-world
```

## Nginx 컨테이너 실행
```
docker run --name hello -d -p 9999:80 nginx
```

## 컨테이너 조회
```
docker ps
```

## 이미지 확인
```
docker images
```

## 브라우저 접속 확인
* ip 변경
```
http://localhost:9999
```

## 로그 확인
```
docker logs -f hello
```

## 컨테이너 Stop/삭제
```
docker stop hello
docker rm hello
```

## 이미지 삭제
```
docker rmi nginx:latest
```
