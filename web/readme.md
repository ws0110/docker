
# Web 실행(base Local Image)

## Image Build
```
docker build -t web:1.0 .
```

## Image 확인
```
docker images
```

## 컨테이너 실행
```
docker run -d --name web -p 80:80 --link app web:1.0
```

## 컨테이너 실행 확인
```
docker ps
```

## 컨테이너 로그 확인
```
docker logs -f web
```

## 접속 확인
* ip 변경
```
http://localhost
```

&nbsp;  
&nbsp;  


# Web 실행(base DockerHub Image)

## Docker 로그인
```
docker login
```

## Image Tag
* 이미지명 앞의 DockerHub 계정정보 변경
```
docker tag web:1.0 jangwisu/web:1.0
```

## Image Push
* 이미지명 앞의 DockerHub 계정정보 변경
```
docker push jangwisu/web:1.0
```

## DockerHub 사이트 Push 이미지 확인

## 기존 web 컨테이너 Clear
* 이미지명 앞의 DockerHub 계정정보 변경
```
docker stop web
docker rm web
docker rmi web:1.0
docker rmi jangwisu/web:1.0
```
## web 컨테이너 실행
* 이미지명 앞의 DockerHub 계정정보 변경
```
docker run -d --name web -p 80:80 --link app jangwisu/web:1.0
```

## 컨테이너 및 이미지 확인
```
docker ps
docker images
docker logs -f web
```

## 접속 확인
* ip 변경
```
http://localhost
```
