

## Image Pull
```
docker pull mariadb:10.0
```

## Image 확인
```
docker images
```

## Volume 디렉토리 생성
```
mkdir /root/data
cd /root/data/
pwd
```

## DB 컨테이너 실행
* -v 옵션의 경로 확인 필요
```
docker run -d \
    --name mariadb \
    -p 3306:3306 \
    -v /root/data/:/var/lib/mysql \
    -e MYSQL_ROOT_PASSWORD=petclinic \
    -e MYSQL_DATABASE=petclinic \
    -e TZ=Asia/Seoul \
    mariadb:10.0

# one line
docker run -d --name mariadb -p 3306:3306 -v /root/data/:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=petclinic -e MYSQL_DATABASE=petclinic -e TZ=Asia/Seoul mariadb:10.0
```

## DB 컨테이너 실행 확인
```
docker ps
```

## 로그 확인
```
docker logs -f mariadb
```

## 접속 확인
```
telnet localhost 3306
```

## Volume 디렉토리 확인
```
ls -al /root/data
```